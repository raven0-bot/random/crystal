const fs = require('fs');

/**
 * Creates a log file. Or a custom file with an custom extension.
 * @function
 * @param {string} name - Name of the file
 * @param {string} content - Content to write to the file
 * @param {string} extension - Custom file extension (THIS IS OPTIONAL)
 */
function createLog(name, content, extension) {
	if (!name) { throw new Error('[Crystal]: ' + 'It is not possible to create a log with an empty name.'); }
	if (!content) { throw new Error('[Crystal]: ' + 'You cannot create an empty log.'); }
	if (extension) {
		const stream = fs.createWriteStream(`${name}.${extension}`, { flags:'a' });
		stream.write(content + '\n');
	}
	else {
		const stream = fs.createWriteStream(`${name}.log`, { flags:'a' });
		stream.write(content + '\n');
	}
}

/**
 * Encodes a string in base64.
 * @function
 * @param {string} str - String to encode
 */
function encode(str) {
	if (!str) throw new Error('[Crystal] No string to encode given');
	const buffer = new Buffer.from(str, 'utf-8');
	const encoded = buffer.toString('base64');

	return encoded;
}

/**
 * Decodes a Base64 encoded string.
 * @function
 * @param {string} str - Base64 encoded string.
 */
function decode(str) {
	if (!str) throw new Error('[Crystal] No string to decode given');
	const buffer = new Buffer.from(str, 'base64');
	const decoded = buffer.toString('utf-8');

	return decoded;
}

module.exports = { createLog, encode, decode };