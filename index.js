module.exports = {
	createLog: require('./src/crystal.js').createLog,
	encode: require('./src/crystal.js').encode,
	decode: require('./src/crystal.js').decode,
};