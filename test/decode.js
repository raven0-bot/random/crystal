const assert = require('chai').assert;
const { decode } = require('../index.js');

/* eslint-disable no-undef */
describe('decode', function() {
	it('should return hello', function() {
		assert.equal(decode('aGVsbG8='), 'hello');
	}).timeout(5000);
});