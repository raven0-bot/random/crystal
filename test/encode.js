const assert = require('chai').assert;
const { encode } = require('../index.js');

/* eslint-disable no-undef */
describe('decode', function() {
	it('should return aGVsbG8=', function() {
		assert.equal(encode('hello'), 'aGVsbG8=');
	}).timeout(5000);
});